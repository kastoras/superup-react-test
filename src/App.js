import React, { Component } from 'react';
import './App.css';
import logo from './assets/logo.png';
import Colors from './Colors.js';

class App extends Component {

    render() {
        return (

            <div className="container">

                <div className="row">
                    <img className="App-logo" alt="App Logo" src={logo} />
                    <h1>Super Up Test</h1>
                </div>                
                <Colors />
            </div>
        );
    }
}

export default App;
