import React, { Component } from 'react';

class Colors extends Component{
    
    constructor(props){
        super(props);
    
        this.state = {
            inputColorHex: '',
            colors : []
        }
     
    }    

    /**
     * Store color and clear input
     *     
     * @param  {String} inputColorHex Color hex string of input
     * @return {void}             
     */
    addColor = (inputColorHex) => {

        const colors = this.state.colors; 

        this.setState({
            inputColorHex: '',
            colors : colors.concat(inputColorHex)
        });

    }

    /**
     * Create value for instyle background
     *     
     * @param  {String} hexColor   Color hex to create background
     * @return {Object}            Object with backround property
     */
    createColor = (hexColor) =>{

        return {
          background: '#'+hexColor,
        };        

    }

    /**
     * Changes state of current input value
     * 
     * @param  {String} options.target   Value of input text - Color Hex
     * @return {Void}
     */
    updateColorInputValue = ({target}) => {

        this.setState({
            inputColorHex: target.value
        });

    }

    /**
     * Creates li list of stored Colors
     *     
     * @return {Array} Array with list of colors
     */
    createColorList = () => {
        const displayList = [];

        this.state.colors.forEach(color => displayList.push(
            <li className="list-group-item" key={color} style={this.createColor(color)}>
                {color}
                <div className="btn-group float-right">
                    <button type="button" onClick={() => this.editColor(color)} className="btn btn-primary">Edit</button>
                    <button type="button" onClick={() => this.deleteColor(color)} className="btn btn-danger">Delete</button>              
                </div>
            </li>
        ));

        return displayList;

    }

    /**
     * Removes color from list and send color to input for edit 
     * 
     * @param  {String} color  Hex color to edit
     * @return {Void}        
     */
    editColor = (color) => {

        const arr = this.removeElementFromArray(color);
        
        this.setState({
            inputColorHex: color,
            colors : arr
        });

    }     

    /**
     * Removes given color from color list
     * 
     * @param  {String} color COlor to remove from list
     * @return {Array}        Array without deleted value
     */
    removeElementFromArray = (color) =>{

        const arr = this.state.colors.filter(function(ele){
           return ele !== color;
        });

        return arr; 
    }

    /**
     * Delete given color from color list
     * 
     * @param  {String} color  Color to remove from list
     * @return {void}
     */
    deleteColor = (color) =>{

        const r = window.confirm("Delete color?");
        if (r === true) {

            const arr = this.removeElementFromArray(color);
            
            this.setState({
                colors : arr
            });
        }

    }


    render(){
        return(
            <div className="row">
                <div className="col-md-6">
                    <div className="col-md-12">
                        <h2>Color HEX</h2>
                    </div>
                    <div className="col-md-6">
                        <form>
                            <div className="form-group">
                                <label htmlFor="colorHex">Enter Color Hex without #</label>
                                <input type="text" className="form-control" placeholder="Enter hex" 
                                    value={this.state.inputColorHex} onChange={evt => this.updateColorInputValue(evt)}></input>
                            </div>                                
                        </form>
                    </div>
                    <div className="col-md-6">
                        <button type="button" onClick={() => this.addColor(this.state.inputColorHex)} className="btn btn-primary">Add Color</button>                        
                    </div>
                </div> 
                
                <div className="col-md-6">
                    <h2>Color List</h2>
                    <ul className="list-group">{this.createColorList()}</ul>
                </div>
            </div>
        );
    }

}

export default Colors;