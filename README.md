This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## ReactJS - Color Management

React app to add - edit - delete colors hex from color list


## Instructions to use applications

Make sure you have a recent copy of Nodejs Installed

## 1. Clone repository

git clone https://kastoras@bitbucket.org/kastoras/superup-react-test.git

## 2. Go into app's directory

cd superup-react-test

## 3. Install Dependencies

npm install

## 4. Start Application Development Mode

npm start

## 5. Open Application

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



## Build Application

npm run build

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

